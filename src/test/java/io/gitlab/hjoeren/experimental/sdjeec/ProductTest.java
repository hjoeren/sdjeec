package io.gitlab.hjoeren.experimental.sdjeec;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.gitlab.hjoeren.experimental.sdjeec.Category.CategoryId;

@SpringBootTest
class ProductTest {

	@Autowired
	Products products;

	// this test doesn't pass
	@Test
	void testFindAllByCategoryAffiliationsCategoryId() {
		products.findAllByCategoryAffiliationsCategoryId(CategoryId.of(UUID.randomUUID()));
	}

	// one-to-one: this test passes
	@Test
	void testFindAllByCategory() {
		products.findAllByCategory(CategoryId.of(UUID.randomUUID()));
	}

	// using UUID instead of CategoryId: this test passes
	@Test
	void testFindAllByCategoryAffiliation2() {
		products.findAllByCategoryAffiliations2(UUID.randomUUID());
	}

}

package io.gitlab.hjoeren.experimental.sdjeec;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "categories")
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED, onConstructor = @__(@Deprecated))
public class Category {

	@EmbeddedId
	@AttributeOverride(name = "productId", column = @Column(name = "id"))
	@Getter
	@Setter(value = AccessLevel.PROTECTED)
	private CategoryId id = CategoryId.of(UUID.randomUUID());

	@Column(name = "name", nullable = false)
	@NonNull
	@Getter
	@Setter
	private String name;

	@Embeddable
	@EqualsAndHashCode
	@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED, onConstructor = @__(@Deprecated))
	@RequiredArgsConstructor(staticName = "of")
	public static class CategoryId implements Serializable {

		private static final long serialVersionUID = -7956680860384594434L;

		@Getter
		private final UUID categoryId;

	}

}

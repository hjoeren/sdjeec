package io.gitlab.hjoeren.experimental.sdjeec;

import org.springframework.data.jpa.repository.JpaRepository;

import io.gitlab.hjoeren.experimental.sdjeec.Category.CategoryId;

public interface Categories extends JpaRepository<Category, CategoryId> {

}

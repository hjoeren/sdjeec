package io.gitlab.hjoeren.experimental.sdjeec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdjeecApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdjeecApplication.class, args);
	}

}

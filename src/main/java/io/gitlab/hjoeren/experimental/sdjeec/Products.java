package io.gitlab.hjoeren.experimental.sdjeec;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import io.gitlab.hjoeren.experimental.sdjeec.Category.CategoryId;
import io.gitlab.hjoeren.experimental.sdjeec.Product.ProductId;

public interface Products extends JpaRepository<Product, ProductId> {

	Iterable<Product> findAllByCategory(CategoryId category);
	
	Iterable<Product> findAllByCategoryAffiliationsCategoryId(CategoryId category);
	
	Iterable<Product> findAllByCategoryAffiliations2(UUID category);

}

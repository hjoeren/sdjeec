package io.gitlab.hjoeren.experimental.sdjeec;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.AttributeOverride;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import io.gitlab.hjoeren.experimental.sdjeec.Category.CategoryId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "products")
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED, onConstructor = @__(@Deprecated))
public class Product implements Serializable {

	private static final long serialVersionUID = 6467664471031172118L;

	@EmbeddedId
	@AttributeOverride(name = "productId", column = @Column(name = "id"))
	@Getter
	@Setter(value = AccessLevel.PROTECTED)
	private ProductId id = ProductId.of(UUID.randomUUID());

	@Column(name = "name", nullable = false)
	@NonNull
	@Getter
	@Setter
	private String name;

	@Embedded
	@AttributeOverride(name = "categoryId", column = @Column(name = "category"))
	@Getter
	@Setter
	private CategoryId category;

	@ElementCollection
	@CollectionTable(name = "category_affiliations", joinColumns = {
			@JoinColumn(name = "product") })
	@AttributeOverride(name = "categoryId", column = @Column(name = "category"))
	@Getter
	@Setter
	private Set<CategoryId> categoryAffiliations = new HashSet<>();

	@ElementCollection
	@CollectionTable(name = "category_affiliations2", joinColumns = {
			@JoinColumn(name = "product") })
	@Column(name = "category")
	@Getter
	@Setter
	private Set<UUID> categoryAffiliations2 = new HashSet<>();

	@Embeddable
	@EqualsAndHashCode
	@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED, onConstructor = @__(@Deprecated))
	@RequiredArgsConstructor(staticName = "of")
	public static class ProductId implements Serializable {

		private static final long serialVersionUID = -924262679264880280L;

		@Getter
		private final UUID productId;

	}

}
